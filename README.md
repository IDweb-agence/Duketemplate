# Duke template

Bienvenue sur le wiki du template Duke, label de musiques electroniques. Vous y retrouverez les étapes de production, les recommandations techniques et la documentation du thême.

## Conventions de nomage et recommandations
- Les noms de variable et de fonctions sont en anglais.
- Tous les commits(depot de fichiers sur le repository) ont un message explicite
- Les préfixes des thême, fonctions et BDD est : dk_
- La feuille de route doit être complétée au fil de l'eau
- Les échéances de la feuille de route doivent être respectée
- L'échéance à 30 jours doit être respectée.

## Liens utiles
- [Feuille de route](https://docs.google.com/spreadsheets/d/1Cm32ZQ-BXLvfQwOcZ7IojVxzB5jU160ZOcIQDPezs4k/edit)
- [Cahier des charges](https://docs.google.com/document/d/17zxki5rfatrPVgRF6xQ5UOmhugOMXKiPUYbh0UALL6s/edit)
- [Arborescence](https://www.mindomo.com/fr/mindmap/b22be11675414fcc962330e21558f3da)
- [fiche artiste](https://docs.google.com/document/d/181fQPLs5C4XXbzIfz-d_Betb84NIExYPCnwZPoDcguA/edit)

# Etapes de production
## [Graphismes](https://docs.google.com/document/d/1-zWLok1Vm6HHa8CwySOHCJUwCDlX5rwNvub9WEdfe-Q/edit)
## [Creation de thême wp](https://docs.google.com/document/d/1G1w1Ks7lalYySi4PJ0VxEtAVpdbCO6oyXDT-hOtTt7M/edit)
## Recherche d'outils
## Insertion de contenus
