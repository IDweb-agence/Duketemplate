<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'duketemplate');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0>ah:tj<qr:B/9:EfpG[x_;*t5KvvjL.[ l]qH(=/,sd26 PR@VN[W9tsz4dM;t}');
define('SECURE_AUTH_KEY',  'KaVN{&j8XbXZIY+Hnxs):[B;$)<|= |(sc)*[7ab_h+o;Cg+W7Pv,pM+q.,:?2fz');
define('LOGGED_IN_KEY',    'Ay)S:Pj$BX6gL!`4Dt}!@-i5aWnEzv| 0a`b_ X<J]~z1K2x<]=J@<wJr`_.Qa(I');
define('NONCE_KEY',        'hR`9#PL$t65di8s&t/O8HIS-;vhOY ~G;M1U&8q!VCG1:fn-DTVjYcnht;.iz{)!');
define('AUTH_SALT',        'P.6$BQSEEH91lJC$V$.1w>39pTDSTucyw%3=!JA(.$;.wb)a`Mlq=fRHo }a{~|_');
define('SECURE_AUTH_SALT', '+FMx2^8_f{ kdsP7u;Ty.!=o2J>O_#S3EnSk^1uB:.d?WrX xQG4mm}t!-y|@hWl');
define('LOGGED_IN_SALT',   'tB}?Twpg-E%.@Gb`O*:,-/lPSDbHt.=t|Zfr&hhJ#CZ Z-%;KwN^&baeUpo&1XNe');
define('NONCE_SALT',       '!Y.hm0>Axl(EJ!)c{ZO0j]p*(KWu0JwyB6FtA>AYb)!vl.aUruF]XppcVD1oPTxi');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'dk_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');